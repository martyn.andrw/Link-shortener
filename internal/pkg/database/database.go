package database

import (
	"context"
	"errors"
	"time"

	"github.com/jackc/pgx/v4"
)

type Logger interface {
	WriteError(error)
	WriteInfo(string)
	WriteOK(string)
}

type Config interface {
	GetDuration() time.Duration
	GetPSQLConn() string
}

type DataBase struct {
	conn *pgx.Conn
	lggr Logger
	cfg  Config
}

func (d *DataBase) Close(ctx context.Context) {
	err := d.conn.Close(ctx)
	if err != nil {
		d.lggr.WriteError(err)
	}
}

func NewDataBase(logger Logger, cfg Config) *DataBase {
	ctx, cancel := context.WithTimeout(context.Background(), cfg.GetDuration())
	defer cancel()

	logger.WriteInfo("trying to connect to database...")
	conn, err := connect(ctx, cfg.GetPSQLConn())
	if err != nil {
		logger.WriteError(err)
		return nil
	}

	logger.WriteOK("Database connection established")

	return &DataBase{
		conn: conn,
		lggr: logger,
		cfg:  cfg,
	}
}

func connect(ctx context.Context, connString string) (conn *pgx.Conn, err error) {
	err = errors.New("failed to connect to database")
	for {
		select {
		case <-ctx.Done():
			return nil, err
		default:
			conn, err = pgx.Connect(context.Background(), connString)
			if err == nil {
				return
			}
		}
	}
}

func (d *DataBase) Add() {}

func (d *DataBase) Get() {}
