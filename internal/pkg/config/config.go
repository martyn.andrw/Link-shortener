package config

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	yaml "gopkg.in/yaml.v2"
)

const (
	configFile = "config.yml"
	reset      = "\033[0m"
	red        = "\033[31m"
	green      = "\033[32m"
	cyan       = "\033[36m"
)

var cfg Config

func init() {
	file, err := os.Open(configFile)
	if err != nil || file == nil {
		log.Fatalf("Failed to open configuration file\n\t%serror%s: %s", red, reset, err)
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("Failed to read configuration file\n\t%serror%s: %s", red, reset, err)
	}

	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		log.Fatalf("Failed to unmarshal\n\t%serror%s: %s", red, reset, err)
	}
}

type Config struct {
	HostWeb      string        `yaml:"host_web"`
	PortWeb      string        `yaml:"port_web"`
	HostPSQL     string        `yaml:"host_psql"`
	PortPSQL     string        `yaml:"port_psql"`
	UserPSQL     string        `yaml:"user_psql"`
	PassPSQL     string        `yaml:"pass_psql"`
	DurationPSQL time.Duration `yaml:"timeout_psql"`
}

func Get() Config {
	return cfg
}

func (c *Config) GetURL() string {
	return fmt.Sprintf("%s:%s", c.HostWeb, c.PortWeb)
}

func (c *Config) GetPSQLConn() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s", c.UserPSQL, c.PassPSQL, c.HostPSQL, c.PortPSQL)
}

func (c *Config) GetDuration() time.Duration {
	return c.DurationPSQL
}
