package web

import (
	"net/http"
)

type Config interface {
	GetURL() string
}

type Logger interface {
	WriteError(error)
	WriteInfo(string)
	WriteOK(string)
}

type App struct {
	mux *http.ServeMux
	cfg Config
	lgg Logger
}

func NewApp(logger Logger, cfg Config) *App {
	logger.WriteInfo("Web application created")
	return &App{
		mux: http.NewServeMux(),
		cfg: cfg,
		lgg: logger,
	}
}

func (a *App) AddNewHandler(pattern string, handler func(http.ResponseWriter, *http.Request)) {
	if []rune(pattern)[0] != '/' {
		pattern = "/" + pattern
	}
	a.mux.HandleFunc(pattern, handler)
}

func (a *App) Start() {
	a.lgg.WriteOK("Web application started")
	err := http.ListenAndServe(a.cfg.GetURL(), a.mux)
	if err != nil {
		a.lgg.WriteError(err)
		return
	}
}
