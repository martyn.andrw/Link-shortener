package mainlogic

// todo
type DataBase interface {
}

type Logger interface {
	WriteError(error)
	WriteInfo(string)
	WriteOK(string)
}

type Mainlogic struct {
	cached *Cache
	db     DataBase
	lggr   Logger
}

func NewMainLogic(database DataBase, logger Logger) *Mainlogic {
	logger.WriteInfo("Main loggic created")
	return &Mainlogic{
		cached: NewCache(),
		db:     database,
		lggr:   logger,
	}
}
