package mainlogic

import (
	"errors"
	"sync"
)

type Cache struct {
	dataActual map[string]string
	dataShort  map[string]string
	muActual   sync.RWMutex
	muShort    sync.RWMutex
	NextIndex  int
}

func (c *Cache) GetActual(short string) (string, bool) {
	c.muActual.RLock()
	defer c.muActual.RUnlock()

	actual, exists := c.dataActual[short]
	return actual, exists
}

func (c *Cache) GetShort(actual string) (string, bool) {
	c.muShort.RLock()
	defer c.muShort.RUnlock()

	short, exists := c.dataShort[actual]
	return short, exists
}

func (c *Cache) Add(actual string) (string, error) {
	c.muActual.Lock()
	c.muShort.Lock()

	defer c.muActual.Unlock()
	defer c.muShort.Unlock()

	short := ConvertToShort(c.NextIndex)
	if _, exists := c.dataActual[short]; exists {
		return "", errors.New("this short link is already taken")
	}

	if link, exists := c.dataShort[actual]; exists {
		return link, errors.New("this actual link is already shortened")
	}

	c.dataActual[short] = actual
	c.dataShort[actual] = short
	c.NextIndex++

	return short, nil
}

var Cached *Cache

func NewCache() *Cache {
	return &Cache{
		dataActual: make(map[string]string),
		dataShort:  make(map[string]string),
	}
}

func init() {
	Cached = NewCache()
}
