package mainlogic

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type Request struct {
	Key  string `json:"key"`
	Link string `json:"link"`
}

type Response struct {
	Result string `json:"result"`
	Error  string `json:"error"`
}

func (m *Mainlogic) IndexHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		// todo get logic
	case "POST":
		var request Request
		body, err := io.ReadAll(r.Body)
		if err != nil {
			m.lggr.WriteError(err)
		}
		defer r.Body.Close()
		json.Unmarshal(body, &request)

		m.lggr.WriteInfo(
			fmt.Sprintf("Received %s-requst. Needed %s link for <%s>", "POST", request.Key, request.Link),
		)

		var result Response
		switch request.Key {
		case "get actual":
			resultLink, ok := Cached.GetActual(request.Link)
			if !ok {
				result.Error = "this short link does not exists"
			}
			result.Result = resultLink
		case "get short":
			resultLink, ok := Cached.GetShort(request.Link)
			if !ok {
				resultLink, err = Cached.Add(request.Key)
				if err != nil {
					m.lggr.WriteError(err)
					result.Error = err.Error()
				}
			}
			result.Result = resultLink
		}

		data, err := json.Marshal(result)
		if err != nil {
			m.lggr.WriteError(err)
		}
		w.Header().Set("Content-Type", "json")
		w.Write(data)
	}
}
