package logger

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

const (
	reset = "\033[0m"
	red   = "\033[31m"
	green = "\033[32m"
	cyan  = "\033[36m"
)

type Logger struct {
	stderr   *bufio.Writer
	stdout   *bufio.Writer
	dataChan chan string
	quitChan chan struct{}
	ticker   time.Ticker
}

func NewLogger() *Logger {
	return &Logger{
		stderr:   bufio.NewWriter(os.Stdout),
		stdout:   bufio.NewWriter(os.Stdout),
		dataChan: make(chan string, 5),
		quitChan: make(chan struct{}, 1),
		ticker:   *time.NewTicker(time.Millisecond * 500),
	}
}

func (l *Logger) WriteError(err error) {
	l.dataChan <- fmt.Sprintf("%s\t%sERROR%s\t%s\n", time.Now().Format(time.UnixDate), red, reset, err)
}

func (l *Logger) WriteInfo(info string) {
	l.dataChan <- fmt.Sprintf("%s\t%sINFO%s\t%s\n", time.Now().Format(time.UnixDate), cyan, reset, info)
}

func (l *Logger) WriteOK(info string) {
	l.dataChan <- fmt.Sprintf("%s\t%sOK%s\t%s\n", time.Now().Format(time.UnixDate), green, reset, info)
}

func (l *Logger) Start() {
	l.WriteOK("logger started")
	for {
		select {
		case <-l.ticker.C:
			er := l.stderr.Flush()
			checkError(er)

			er = l.stdout.Flush()
			checkError(er)

		case <-l.quitChan:
			info := "logger has ended its work"
			_, er := l.stdout.WriteString(fmt.Sprintf("%s\t[%sINFO%s]\t%s\n", time.Now().Format(time.UnixDate), cyan, reset, info))
			checkError(er)

			er = l.stdout.Flush()
			checkError(er)

			er = l.stderr.Flush()
			checkError(er)
		default:
		}

		select {
		case msg := <-l.dataChan:
			if strings.Contains(msg, "INFO") || strings.Contains(msg, "OK") {
				_, err := l.stdout.WriteString(msg)
				checkError(err)
			}
			if strings.Contains(msg, "ERROR") {
				_, err := l.stderr.WriteString(msg)
				checkError(err)
			}
		default:
		}
	}
}

func (l *Logger) End() {
	l.quitChan <- struct{}{}
}

func checkError(err error) {
	if err != nil {
		log.Printf("logger got %serror%s: %s\n", red, reset, err)
	}
}
