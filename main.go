package main

import (
	"context"
	"flag"

	"github.com/Kexibt/Link-shortener/internal/pkg/config"
	"github.com/Kexibt/Link-shortener/internal/pkg/database"
	"github.com/Kexibt/Link-shortener/internal/pkg/logger"
	mainlogic "github.com/Kexibt/Link-shortener/internal/pkg/main_logic"
	"github.com/Kexibt/Link-shortener/internal/pkg/web"
)

var cfg Config

type Config struct {
	config.Config
	DB string
}

func init() {
	cfg.Config = config.Get()
	flag.StringVar(&cfg.DB, "db", "psql", "which database to use(psql/todo)")
}

func main() {
	flag.Parse()

	loggr := logger.NewLogger()
	go loggr.Start()

	db := database.NewDataBase(loggr, &cfg)
	defer db.Close(context.Background())

	mainl := mainlogic.NewMainLogic(db, loggr)
	app := web.NewApp(loggr, &cfg)

	app.AddNewHandler("/", mainl.IndexHandler)
	app.Start()
}
